import aioredis
import aiopg

async def setup_redis(bot):
    bot.redis = await aioredis.create_redis_pool('redis://localhost')

async def setup_postgres_pool(bot):
    bot.pg_pool = await aiopg.create_pool(POSTGRES_AUTH)

# Prefix used for bot commands
BOT_PREFIX = ("v?", "v!")

# Default disord.GUILD.id to use
GUILD_ID = 353694095220408322

# Snap Purple
COLOR = 7821292

# What plugins to load from the `plugins` directory
COGS = (
    'plugins.meme',
#    'plugins.roles_sub',
    'plugins.notifications',
    'plugins.loaders',
    'plugins.messages',
    'plugins.alt_checker',
    'plugins.lmgtfy',
    'plugins.qotd',
    'plugins.utilities',
    'plugins.escaperoom',
    'plugins.among_us',
)

# The number of messages discord.py stores in memory
MAX_MESSAGES = 5000

# Discord Token for bot authentication
TOKEN = ''

POSTGRES_AUTH = 'dbname=<db_name> user=<db_user> password=<db_password> host=127.0.0.1'

custom_emoji = [
    ["<:zoomeyes:695120566696411236>", "zoomeyes", ],
    ["<:youtried:695120562531336212>", "youtried", ],
    ["<:yikes:695120566134243471>", "yikes", ],
    ["<:xilwithit:695120564720893953>", "xilwithit", ],
    ["<:xilasspy:695120566469787668>", "xilasspy", ],
    ["<:xilasheart:695120565677326387>", "xilasheart", ],
    ["<:xilasdangerous:695120566839148555>", "xilasdangerous", ],
    ["<:xilascookie:695120567061446696>", "xilascookie", ],
    ["<:wutpat:695120565412823082>", "wutpat", ],
    ["<:why:695120566461530132>", "why", ],
    ["<:TrailerDrake:695120565199044691>", "TrailerDrake", ],
    ["<:tired:695120558848737300>", "tired", ],
    ["<:tips_hat:695120563974438960>", "tips_hat", ],
    ["<:thonk:695120559171960902>", "thonk", ],
    ["<:thinkpat:695120564632813638>", "thinkpat", ],
    ["<:TheoryTheory:735924393397583995>", "TheoryTheory", ],
    ["<:TheMoreYouKnow:695120556181422170>", "TheMoreYouKnow", ],
    ["<:tastethis:695120565719269466>", "tastethis", ],
    ["<:SweatPat:695120565253439490>", "SweatPat", ],
    ["<:sus_think:695120561352736870>", "sus_think", ],
    ["<:suresure:695120565882847333>", "suresure", ],
    ["<:SuperStar:695120560258285690>", "SuperStar", ],
    ["<:StrangeStarDamage:695120565174009896>", "StrangeStarDamage", ],
    ["<:StrangeSkeptic:695120564242743357>", "StrangeSkeptic", ],
    ["<:StrangeSignificant:695120564666499122>", "StrangeSignificant", ],
    ["<:StrangeShook:695120563840090152>", "StrangeShook", ],
    ["<:StrangeHug:695120565706424390>", "StrangeHug", ],
    ["<:aniguns:695120564108394496>", "aniguns", ],
    ["<:AWAUGERY:695120557036929086>", "AWAUGERY", ],
    ["<:ArcEyes:695120553027174410>", "ArcEyes", ],
    ["<:BanHammer:695120553803120701>", "BanHammer", ],
    ["<:birbe:695120552611807313>", "birbe", ],
    ["<:blobkiss:695120553345941535>", "blobkiss", ],
    ["<:BookTheory:721427272510210059>", "BookTheory", ],
    ["<:brain_heart:695120552808939550>", "brain_heart", ],
    ["<:ChaoMoose:706728248796643369>", "ChaoMoose", ],
    ["<:chronicAngry:695120560837099570>", "chronicAngry", ],
    ["<:chronicAnnoyed:695120560803282975>", "chronicAnnoyed", ],
    ["<:chronicDisinterest:695120559536734279>", "chronicDisinterest", ],
    ["<:chronicEmbarrassed:695120561151410176>", "chronicEmbarrassed", ],
    ["<:chronicFlirt:695120560862265384>", "chronicFlirt", ],
    ["<:chronicHumble:695120561080369172>", "chronicHumble", ],
    ["<:chronicPardon:695120561562452008>", "chronicPardon", ],
    ["<:chronicRage:695120561918967857>", "chronicRage", ],
    ["<:chronicUncomfy:695120561684348958>", "chronicUncomfy", ],
    ["<:ClydeFight:695120553899458594>", "ClydeFight", ],
    ["<:ConceitedReaction:695120562506170429>", "ConceitedReaction", ],
    ["<:confused_screaming:695120563848609823>", "confused_screaming", ],
]

ROLE_PERMS = [
    {
        "name": "Divine Theorist",
        "perms": "•**Prestige?** You can't prestige until you reach <@&514115149158678550>. \n"
    },
    {
        "name": "Ascended Theorist",
        "perms": []
    },
    {
        "name": "Sage Theorist",
        "perms": "•**Use !degendex?** You can't use !degendex until you reach <@&514115324841426954>. \n"
    },
    {
        "name": "Pure Theorist",
        "perms": []
    },
    {
        "name": "Legendary Theorist",
        "perms": "•**Go Live?** You can't Go Live in a VC until you reach <@&514115410396839955>. \n"
    },
    {
        "name": "Limitless Theorist",
        "perms": "•**Post in warn reports?** You can't post in <#624257664125501450> until you reach <@&514115613468393473>. \n"
    },
    {
        "name": "Inhuman Theorist",
        "perms": []
    },
    {
        "name": "Master Theorist",
        "perms": "•**Use !sleeping-chat?** You can't use !sleeping-chat until you reach <@&514115743151816714>. \n"
    },
    {
        "name": "Genius Theorist",
        "perms": []
    },
    {
        "name": "Enlightened Theorist",
        "perms": []
    },
    {
        "name": "Insane Theorist",
        "perms": "•**See the mod preview channel?** You can't see <#519873222506709002> until you reach <@&514116251723890732>. \n"
    },
    {
        "name": "Obsessed Theorist",
        "perms": "•**Post in daily positivity?** You can't post in <#565566736800284683> until you reach <@&514116287283331073>. \n"
    },
    {
        "name": "Addicted Theorist",
        "perms": [
            "•**Use !duar-loss-of-innocence?** You can't use !duar-loss-of-innocence until you reach <@&514116320649019402>. \n",
            "•**Use !high-role?** You can't use !high-role until you reach <@&514116320649019402>. \n",
        ]
    },
    {
        "name": "Dedicated Theorist",
        "perms": [
            "•**Use !fall-of-duar?** You can't use !fall-of-duar until you reach <@&514116355763732480>. \n",
            "•**Be recommended for Approved roles?** You can't be recommended for Approved roles until you reach <@&514116355763732480>. \n",
        ]
    },
    {
        "name": "Loyal Theorist",
        "perms": "•**Send suggestions?** You can't suggest in the Suggestions channels until you reach <@&514116391607992332>. \n"
    },
    {
        "name": "Serious Theorist",
        "perms": "•**Change my nickname?** You can't change your nickname until you reach <@&514114232011194379>. \n"
    },
    {
        "name": "Hard-Working Theorist",
        "perms": [
            "•**Attach files?** You can't attach files (outside of <#354405657484460043>, <#528253600682737685>, <#353766146115371009> and the Theorizing channels) until you reach <@&514114175409061900>. \n",
            "•**Embed links?** You can't embed links (outside of <#354405657484460043>, <#528253600682737685>, <#353766146115371009> and the Theorizing channels) until you reach <@&514114175409061900>. \n",
            "•**See suggestion channels?** You can't see the Suggestions channels until you reach <@&514114175409061900>. \n",
        ]
    },
    {
        "name": "Improving Theorist",
        "perms": [
            "•**Use emojis from other servers?** You can't use external emoji until you reach <@&514114132392411157>. \n",
            "•**Use !yeet?** You can't use !yeet until you reach <@&514114132392411157>. \n",
        ]
    },
    {
        "name": "Rookie Theorist",
        "perms": [
            "•**Add reactions?** You can't add reactions until you reach <@&514114064608264205>. \n",
            "•**Send images?** You can only send images in the Theorizing Channels once you reach <@&514114064608264205>. \n"
        ]
    },
    {
        "name": "New Theorist",
        "perms": "•**Send images?** You can only send images in <#353766146115371009> and <#528253600682737685> once you reach <@&514113781903917056>. \n"
    },
]
APPROVED_ROLES = ['Mods', 'Server Glue', 'Approved Theorizer', 'Approved Artist',
        'Approved Writer', 'Approved Musician', 'Expert Meme-er',]
