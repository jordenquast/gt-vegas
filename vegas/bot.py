import datetime
import sys
import traceback

from discord import Intents
from discord import Forbidden
from discord import Game
from discord.ext.commands import Bot
from discord.ext.commands.errors import MissingAnyRole

from settings.local_settings import BOT_PREFIX
from settings.local_settings import COGS
from settings.local_settings import MAX_MESSAGES
from settings.local_settings import setup_redis
from settings.local_settings import setup_postgres_pool


class VEGAS(Bot):
    """Base VEGAS bot"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_time = datetime.datetime.now()

        # Gets the 'plugins' directory
        for cog in COGS:
            try:
                self.load_extension(cog)
            except Exception as e:
                print(f'Failed to load extension {cog}.', file=sys.stderr)
                traceback.print_exc()

        self.loop.run_until_complete(setup_redis(self))
        self.loop.run_until_complete(setup_postgres_pool(self))


    async def on_ready(self):
        """Default startup command"""
        print('Bot on_ready()')
        await client.change_presence(activity=Game(name="with development"))
        print("Logged in as " + client.user.name)

    async def on_error(self, event, ctx, error, **kwargs):
        """Default Error Handling for whole bot.
        If your method/cog needs a custom error message,
           create a custom Exception, and match against it here!"""
        if isinstance(error, MissingAnyRole):
            await ctx.send("You don't have the roles to do that!")
        else:
            print(error)
        raise error

    async def on_command_error(self, event, ctx, error):
        print(error)

    async def dm_author(self, ctx, message):
        """Helper method to direct message the user that sent the message"""
        try:
            await self.send_message(ctx.message.author, message)

        except Forbidden:
            await self.send_message(message)



intents = Intents.default()  # All but the two privileged ones
intents.members = True  # Subscribe to the Members intent
client = VEGAS(messages=MAX_MESSAGES, command_prefix=BOT_PREFIX, case_insensitive=True, intents=intents)
