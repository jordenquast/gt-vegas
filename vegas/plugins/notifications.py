"""This Cog provides user and Disaster Director access to roles.

"""
import datetime
import asyncio
import discord
from discord.utils import get
from discord.ext import commands
from discord.ext import tasks

from discord.ext.commands import has_any_role

from settings.base import custom_emoji

from .base import BasePlugin

from settings.local_settings import GUILD_ID

DD_ROLE_FILTER = ("no", "muted", "notification", "deadlock",
                  "has read the pinned doc in arg",
                  "haven't read the rules",
                  "lamp'd", "straight a's!",
                  "birthday", "official stream commentator", "(dd)",)

ROLE_FILTER = ("notification", "she/her", "he/him", "they/them", "ask pronouns", "(all)", )
PRONOUN_FILTER = ("she/her", "he/him", "they/them", "ask pronouns", )
APPROVED_FILTER = ("approved", "mods", "expert", "theorist", "glue", "disaster", )

INFO_ID = 735635802960429087
HELP_ID = 516027780908056578

class NotificationPlugin(BasePlugin):
    """This COG provides an interface for GenPop and DD's to interact with roles

    Attributes:
    -----------
    self.bot: discord.ext.commands.Bot
        References the "Bot"

    Methods
    -------
    _build_roles_lists(roles=discord.Role)
        This `private` method returns a list of roles from the guild that meet the filter requirements.`
    _give_role(context=discord.ext.commands.Context, username=discord.User.mention, rolename=str)
        This `private` method searches the guild provided in `context` for `rolename` and adds the role to `username`.
    give(context=discord.ext.commands.Context, username=discord.User.mention, rolename=str)
        This command method allows `Mods` and `DDs` to give users a select role that meets the filters.
    join_role(context=discord.ext.commands.Context, username=discord.User.mention, rolename=str)
        This command method allows any user to get a role that meets the filters.
    leave_role(context=discord.ext.commands.Context, username=discord.User.mention, rolename=str)
        This command method allows any user to remove a role that meets the filters.
    list(context=discord.ext.commands.Context)
        This method displays a list of all the roles
    notifications(context=context.ext.commands.Context)
        This command methods allows any user to display an interactive discord.Embed to get or remove roles.
    on_raw_reaction_add(payload=discord.RawReactionActionEvent)
        This event method adds and removes roles to users based on `self.notifications`.
    remove_role(context=discord.ext.commands.Context, username=discord.User.mention, rolename=str, reason=str)
        This command method allows `Mods` and `DDs` to remove a role that meets the filters.
    temp_give_role(context=discord.ext.commands.Context, username=discord.User.mention, time=str, rolename=str, reason=str)
        This command method allows `Mods` and `DDs` to give users a select role that meets the filters for a specified period of time.

    """

    def __init__(self, bot):
        super().__init__(bot)
        self.bot = bot
        self.role_timeout.start()

    def get_ping_chan(self):
        guild = self.bot.get_guild(GUILD_ID)
        return get(guild.text_channels, name="role-request")

    @commands.Cog.listener()
    async def on_ready(self):
        print("Notification READY!")
        

    @tasks.loop(minutes=1.0)
    async def role_timeout(self):
        await self.bot.wait_until_ready()
        now = datetime.datetime.now()
        select_sql = "SELECT * FROM discord_user_discordrole AS r LEFT JOIN discord_user_discorduser AS u ON r.discord_user_id=u.id WHERE r.removed='f' AND date_expired<NOW();"
        close_sql = "UPDATE discord_user_discordrole SET removed='t', reason=%s WHERE id=%s"

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(select_sql)
                ret = await cur.fetchall()

        for temp_role in ret:
            server = self.bot.get_guild(int(temp_role[8]))
            role = server.get_role(int(temp_role[4]))
            member = server.get_member(int(temp_role[9]))
            reason = temp_role[2]
            await member.remove_roles(role)
            async with self.bot.pg_pool.acquire() as uconn:
                async with uconn.cursor() as ucur:
                    reason += ". Role Removed from timeout at " + str(datetime.datetime.now())
                    await ucur.execute(close_sql, (reason, temp_role[0]))
            notif_chan = self.get_ping_chan()
            await notif_chan.send("{} removed from {}".format(role.name, member.mention))

    async def _check_role_exists(self, context, role_name):
        """This method checks if a role exists in a guild and returns it. If the role doesn't exist, `None` is returned.

        Parameters
        ----------
        context: discord.Context
            The context to use.
        role_name: str
            The role name to lookup.

        Returns
        -------
        None
            No role was found.
        discord.Role
            The role requested.
        """
        if any(word.lower() in role_name.lower() for word in DD_ROLE_FILTER):
            role = get(context.guild.roles, name=role_name)
            if not role:
                await context.send("I can't find that role.")
                return None
            return role
        else:
            await context.send("Must be an approved role!")

    async def _build_roles_lists(self, context, roles):
        '''
        Placeholder lists for each of the "sections" of self roles.

        The final list data structure will follow the format:

        role_list = [
            ["Role Name", custom_emoji[i]],
            ...
            ...
        ]
        '''
        notif_roles = []
        pronoun_roles = []
        all_roles = []
        selfrole_counter = 0  # used to loop through the custom_emoji list independantly
        custom_emoji_list = [reaction for reaction in context.guild.emojis if not reaction.animated]

        '''
            Loops through all roles in the server
                If the role fits a filter, the role and an emoji are added to one of the role lists
                the self role counter is incremented
        '''
        for role in roles:
            if "notification" in role.name.lower():
                notif_roles.append([role.name, custom_emoji_list[selfrole_counter]])
                selfrole_counter += 1
            if any(pronoun_name.lower() in role.name.lower()
                   for pronoun_name in PRONOUN_FILTER):
                pronoun_roles.append([role.name, custom_emoji_list[selfrole_counter]])
                selfrole_counter += 1
            if "(all)" in role.name.lower():
                all_roles.append([role.name, custom_emoji_list[selfrole_counter]])
                selfrole_counter += 1
        return notif_roles, pronoun_roles, all_roles, selfrole_counter

    async def _ensure_user_exists(self, db_cursor, user_id, guild_id):
        """Private method that checks if a user exists and if the user does not, adds an entry.

        Parameters
        ----------
        db_cursor: aiopg.Connection.cursor
            An active cursor to aiopg. Any db connection with an `execute` and `fetchone` method matching aiopg's implemention can be sub'd
        user_id: str
            The discord user id of the user to lookup
        guild_id: str
            The guild/server id associated with this user

        """
        select_sql = "SELECT * FROM discord_user_discorduser WHERE discord_user_id=%s AND discord_guild_id=%s"
        insert_sql = "INSERT INTO discord_user_discorduser(discord_guild_id, discord_user_id) VALUES(%s, $s)"

        await db_cursor.execute(select_sql, (user_id, guild_id))
        user = await db_cursor.fetchone()

        if not user:
            await db_cursor.execute(select_sql, (guild_id, user_id))

    @commands.group(description="Get added or removed to notification roles",
                    case_insensitive=True,
                    aliases=['n', 'noti', ])
    async def notifications(self, context):
        if context.invoked_subcommand is None:

            caller_roles = [role.name.lower()
                            for role in context.message.author.roles]
            color = context.message.author.top_role.color
            roles = context.message.guild.roles

            notif_roles, pronoun_roles, all_roles, selfrole_counter = await self._build_roles_lists(context, roles)

            desc = '''GTD has many roles you can recieve yourself!
            Use this bot command to get them! Just react to this embed to get role you want!
            ❌ to close.'''

            embed = discord.Embed(title="Self Roles",
                                  description=desc,
                                  colour=color)

            if notif_roles:
                notif_out = ""
                i = 1
                for role in notif_roles:
                    notif_out += "{} {}\n".format(role[1], role[0])

                    if len(notif_out) >= 860:
                        embed.add_field(name="Notification Roles " + str(i), value=notif_out,
                                        inline=False)
                        i += 1
                        notif_out = ""

                embed.add_field(name="Notification Roles", value=notif_out,
                                inline=False)

            if pronoun_roles:
                pronoun_out = ""
                for role in pronoun_roles:
                    pronoun_out += "{} {}\n".format(role[1], role[0])
                embed.add_field(name="Pronoun Roles", value=pronoun_out,
                                inline=False)
            if all_roles:
                all_out = ""
                for role in all_roles:
                    all_out += "{} {}\n".format(role[1], role[0])
                embed.add_field(name="All Roles", value=all_out,
                                inline=False)

            embed_message = await context.send(embed=embed)

            def check(reaction, user):
                return user == context.message.author

            wait = True
            while wait:
                try:
                    reaction, user = await self.bot.wait_for('reaction_add', timeout=60.0, check=check)
                    if str(reaction.emoji) == "❌":
                        wait = False
                        await embed_message.edit(content="Thanks for using VEGAS!", embed=None)
                        break
                except asyncio.TimeoutError:
                    wait = False
                    # await embed_message.edit(content="Thanks for using VEGAS!", embed=None)
                else:
                    pickable_roles = notif_roles + pronoun_roles + all_roles
                    give_role = None
                    for role in pickable_roles:
                        if str(reaction) == str(role[1]):
                            give_role = get(context.guild.roles, name=role[0])
                    if give_role:
                        if give_role in user.roles:
                            action = "removed"
                            await user.remove_roles(give_role)
                        else:
                            action = "added"
                            await user.add_roles(give_role)
                        await context.send(
                            "Thanks for using self roles! The {} role was {}!".format(give_role.name, action))
                        wait = False

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        channel = self.bot.get_channel(payload.channel_id)
        if not isinstance(channel, discord.DMChannel):
            if channel.name == "bot-spam" or channel.name == "vegas-logging":
                message = await channel.fetch_message(payload.message_id)
                if message.content == "" and (
                        message.author.id == 691420795473494046 or message.author.id == 376171158497918976):
                    notif_roles, pronoun_roles, all_roles, selfrole_counter = await self._build_roles_lists(context,
                                                                                                            message.guild.roles)
                    give_role = None
                    for role in notif_roles:
                        if str(payload.emoji) == str(role[1]):
                            give_role = get(message.guild.roles, name=role[0])
                    for role in pronoun_roles:
                        if str(payload.emoji) == str(role[1]):
                            give_role = get(message.guild.roles, name=role[0])
                    for role in all_roles:
                        if str(payload.emoji) == str(role[1]):
                            give_role = get(message.guild.roles, name=role[0])
                    if give_role:
                        if give_role in message.author.roles:
                            await payload.member.remove_roles(give_role)
                        else:
                            await payload.member.add_roles(give_role)
                        await channel.send("Thanks for using Self Roles! The {} role was toggled".format(give_role.name))
                        await message.clear_reactions()

    @notifications.command(name='temp_give',
                           description='Temporarily give a notification/no role',
                           brief='Temporarily give a notification role (Disaster Director Only)',
                           aliases=['tg', ])
    @has_any_role('Mods', 'Disaster Director')
    async def temp_give_role(self, context, username, timeout, role_name, *reason):
        """Allows Mods and Disaster Directors to temporarily give a discord.Role to a member.

        Paramteres
        ----------
        context: discord.Context
            The generic discord context parameter that gets passed with every command
        username: str
            A discord.User.mention
        timeout: str
            The amount of time to give therole for.
            The format for `timeout` is as follows:
                - 10m = ten minutes
                - 2h = two hours
                - 7d = seven days
                - 1w = one week
                - 2M = two months
        role_name: str
            The discord.Role.name to add to the member
        reason: str
            The reason the member recieved this role. Only used for logging purposes.

        """
        sql = """
        INSERT INTO discord_user_discordrole(date_recieved, reason, date_expired, role_id, discord_user_id, removed)
        VALUES(%s, %s, %s, %s, %s, 'f')
        """
        user = await self._give_role(context, username, role_name, *reason)
        guild = context.message.guild
        role = get(context.guild.roles, name=role_name)
        discord_user_id = user.id
        reason = " ".join(reason)
        now = datetime.datetime.now()

        time_format = timeout[-1]
        time_str_len = len(timeout) - 2
        time = int(timeout[time_str_len])

        if time_format == "m":
            time_offset = datetime.timedelta(minutes=time)
            time_format = "minutes"

        elif time_format == "h":
            time_offset = datetime.timedelta(hours=time)
            time_format = "hours"

        elif time_format == "d":
            time = datetime.timedelta(days=time)
            time_format = "days"

        elif time_format == "w":
            time_offset = datetime.timedelta(weeks=time)
            time_format = "weeks"

        elif time_format == "M":
            mtime = time * 4
            time_offset = datetime.timedelta(weeks=mtime)
            time_format = "months"

        removal_time = now + time_offset

        sql_insert = sql.format(int(discord_user_id), removal_time)

        user_id_link = await self._check_user_exists(user.id, guild.id)
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                insterted = await cur.execute(sql_insert, (now, reason, removal_time, role.id, user_id_link))
                await cur.execute(insterted)
                fetched = await cur.fetchall()
                await context.send("Temporary Role `{rolename}` logged for removal on {removal_time}  GMT-4".format(
                    rolename=role_name,
                    removal_time=removal_time.strftime("%b %-d, %y %H:%M")))

    @notifications.command(name='give',
                           description='Give a notification role',
                           brief='Give a notification role (Disaster Director Only)',
                           aliases=['g', ])
    @has_any_role('Mods', 'Disaster Director')
    async def give(self, context, username, role_name, *reason):
        sql = """
        INSERT INTO discord_user_discordrole(date_recieved, reason, date_expired, role_id, discord_user_id, removed)
        VALUES(%s, %s, %s, %s, %s, 'f')
        """
        reason = " ".join(reason)
        reason = "Given Reason: " + reason
        role = get(context.guild.roles, name=role_name)
        now = datetime.datetime.now()
        removal_time = None
        user = await self._give_role(context, username, role_name, *reason)
        guild = context.message.guild

        user_id_link = await self._check_user_exists(user.id, guild.id)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(sql, (now, reason, removal_time, role.id, user_id_link))

    async def _give_role(self, context, username, role_name, *reason):
        author = context.message.author
        user_id = int(context.message.mentions[0].id)

        user = get(context.message.guild.members, id=user_id)

        if role_name:
            role = await self._check_role_exists(context, role_name)

        if role:
            try:
                await user.add_roles(role, reason="Given roles by {}".format(author.mention))
                await context.send("Added {} to {}".format(
                    role.name,
                    user.mention
                ))
                return user
            except:
                await context.send("I don't think I can do that...")

    @notifications.command(name='remove',
                           description='Remove a notification role',
                           brief='Remove a notification role (Disaster Director Only)',
                           aliases=['r', ])
    @has_any_role('Mods', 'Disaster Director')
    async def remove_role(self, context, username, role_name, *reason):
        close_sql = "UPDATE discord_user_discordrole SET removed='t', reason=r.reason || %s, date_expired=%s FROM discord_user_discordrole AS r LEFT JOIN discord_user_discorduser AS u ON u.id=r.discord_user_id WHERE r.role_id='%s' AND u.discord_user_id='%s' AND u.discord_guild_id='%s'"
        user_id = int(context.message.mentions[0].id)
        user = get(context.message.guild.members, id=user_id)
        guild = context.message.guild
        reason = " ".join(reason)
        reason = "\nRemoval  Reason: " + reason + "\n"
        user_id_link = await self._check_user_exists(user.id, guild.id)

        if role_name:
            role = await self._check_role_exists(context, role_name)
            author = context.message.author

        if role:
            try:
                await user.remove_roles(role, reason="Role removed by {}".format(author.mention))
                await context.send("Removed {} from {}.".format(
                    role.name,
                    user.mention
                ))
            except:
                await context.send("I dont think I can do that...")

            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cur:
                    await cur.execute(close_sql, (reason, datetime.datetime.now(), role.id, user.id, guild.id))

    @notifications.command(name='join',
                           description='Join a notification role',
                           brief='Join a notification role')
    async def join_role(self, context, *role_name):
        info_channel = self.bot.get_channel(INFO_ID)
        help_channel = self.bot.get_channel(HELP_ID)
        if context.message.channel.id == 498298844392718346:
            return
        if role_name:
            role_name = " ".join(role_name)
            role = get(context.guild.roles, name=role_name)
            author = context.message.author
            if any(word.lower() in role_name.lower() for word in ROLE_FILTER):
                await author.add_roles(role, reason="Self Role from VEGAS")
                await context.send("Added {} to the role {}".format(
                    author.mention,
                    role.name
                ))
            elif any(word.lower() in role_name.lower() for word in APPROVED_FILTER):
                await context.send("You can not add that role to yourself. Please take a couple minutes and read {}.\nIf you're still confused afterwards feel free to ask in {}.".format(
                    info_channel.mention,
                    help_channel.mention
                ))
                context['args'] = ['notifications', 'temp_give_role', context.author.mention, "20m", "Muted", "User attempted to add approved/leveled Role"]
                self.bot.invoke(context)
            else:
                await context.send("Foolish humans... Make sure you are picking a role from list")

    @notifications.command(name='leave',
                           description='Leave a notification role',
                           brief='Leave a notification role',
                           )
    async def leave_role(self, context, *role_name):
        if role_name:
            role_name = " ".join(role_name)
            role = get(context.guild.roles, name=role_name)
            author = context.message.author
            if any(word.lower() in role_name.lower() for word in ROLE_FILTER):
                await author.remove_roles(role, reason="Selfrole from VEGAS")
                await context.send("Removed {} from the role {}".format(
                    author.mention,
                    role.name
                ))
            else:
                await context.send("Foolish humans... Make sure you are picking a role from list")

    @notifications.command(name='list',
                           descriptions='List all of the current joinable roles',
                           brief='List all of the current joinable roles',
                           aliases=['l', ])
    async def list(self, context):
        roles = context.message.guild.roles
        output = "```[Notification Roles]\n\n"
        for role in roles:
            if "notification" in role.name.lower():
                output += role.name + "\n"
        output += "```"
        await context.send(output)


def setup(bot):
    bot.add_cog(NotificationPlugin(bot))
