import asyncio
import discord
from discord.utils import get
from discord.ext import commands
from discord.ext.commands import has_any_role
from discord import ActivityType

from settings.base import ROLE_PERMS
from .base import BasePlugin
from checks import is_owner


LOG_CHANNEL_NAME = 'vegas-logging'

APPROVED_ROLES = [
    "Mods",
    "Disaster Director",
    "Server Glue",
    "Approved Theorizer",
    "Approved Artist",
    "Approved Writer",
    "Approved Musician",
    "Expert Meme-er",
]

BAN_APPEAL = 'https://bit.ly/GTD-Ban-Appeal'


class UtilitiesPlugin(BasePlugin):
    def event_logging_channel(self, channels):
        return get(channels, name=LOG_CHANNEL_NAME)

    @commands.command(description="Update bot activity",
                      case_insensitive=True)
    @is_owner()
    async def status(self, context: discord.ext.commands.Context, status_type: str, status_url: str, *status_message: str) -> None:
        """Updates the bot status"""
        status_message = " ".join(status_message)
        lst = status_type.lower()
        if "watching" in lst:
            await self.bot.change_presence(activity=discord.Activity(type=ActivityType.watching, name=status_message))
        elif "streaming" in lst:
            await self.bot.change_presence(activity=discord.Streaming(name=status_message, url=my_twitch_url))
        elif "gaming" in lst:
            await self.bot.change_presence(activity=discord.Game(name=status_message))
        elif "listening" in lst:
            await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name=status_message))
        else:
            await self.bot.change_presence(activity=discord.Activity(name=status_message))


    @has_any_role("Mods", "Disaster Director")
    @commands.command(description="Give all the approved roles Trello Notification",
                      case_insensitive=True,
                      aliases=['annoy-staff',])
    @has_any_role("Mods", "Disaster Director")
    async def trello_notif(self, context):
        trello_notif = get(context.guild.roles, name="Trello (Staff) Notification")
        all_approveds = set()
        roles = []
        for role in APPROVED_ROLES:
            approved_role = get(context.guild.roles, name=role)
            await context.send("Collecting Members for: {}".format(
                approved_role.name
            ))
            all_approveds.update(approved_role.members)

        await context.send("Adding Trello Notif role...")

        for approved in all_approveds:
            if trello_notif.name not in approved.roles:
                tmpl = "Giving {} Trello Notification!".format(approved.name)
                msg = await context.send(tmpl)
                await approved.add_roles(trello_notif)
                await msg.add_reaction("🌈")
            

    @commands.command(description="Returns all of the users that have a \
                                   specific role",
                      case_insensitive=True)
    @has_any_role('Mods', 'Disaster Director', 'Subreddit Moderator',
                  'Server Glue')
    async def rolemembers(self, context, roleArg=None):
        role_list = [role.name for role in context.message.guild.roles]
        if roleArg is None:
            await context.send("You didn't add a role.")
        else:
            newRole = get(context.guild.roles, name=roleArg)
            if newRole.name.lower() in [x.lower() for x in role_list]:
                memberNames = [x.mention for x in newRole.members]
                title = 'There is ' + str(len(newRole.members)) + ' Member(s) with this role.'
                colour = newRole.color
                embed = discord.Embed(title=title, colour=colour)
                field_value = ""
                field_amt = 0

                for member in memberNames:
                    if len(field_value) + len(member) >= 1000:
                        embed.insert_field_at(field_amt, name="Members with " + newRole.name, value=field_value, inline=True)
                        field_value = ""
                        field_amt += 1
                    field_value += "\n-" + member
                if field_value != "":
                    embed.insert_field_at(field_amt, name="Members with " + newRole.name, value=field_value, inline=True)
                await context.send(embed=embed)
            else:
                await context.send("That argument isn't in the rolelist.")



    @commands.command(description="Gives a brief description of what leveled permissions a user doesn't have", case_insensitive=True)
    async def why_cant_i(self, context, user=None, filter=None):
        if context.message.mentions:
            user = context.message.mentions[0]
        else:
            filter = user
            user = context.author

        theoristRole=None
        theoristMention=None
        theoristNumber=0
        theoristPerms=[]
        theoristColor=user.top_role.color

        for dictRole in ROLE_PERMS:
            for role in user.roles:
                if role.name == dictRole["name"]:
                    theoristRole=role
                    theoristMention=role.mention
                    theoristColor=role.color
                    break
            if theoristRole == None:
                if isinstance(ROLE_PERMS[theoristNumber]["perms"], list):
                    for item in ROLE_PERMS[theoristNumber]["perms"]:
                        theoristPerms.insert(0, item)
                else:
                    theoristPerms.insert(0, ROLE_PERMS[theoristNumber]["perms"])
                theoristNumber += 1

        embed = discord.Embed(title="Why Can't I...? A Guide to Leveled Perms", description="This command tells you what leveled permissions you can't currently use. For a more specific result, add a search term after v!why_cant_i.", colour=theoristColor)
        embed.add_field(name="Your Role", value=theoristMention, inline=False)
        field_value = ""
        field_amt = 0

        if filter == None:
            for entry in theoristPerms:
                if len(field_value) + len(entry) >= 1024:
                    embed.insert_field_at(field_amt, name="Permissions", value=field_value, inline=False)
                    field_value = ""
                    field_amt += 1
                field_value += entry
            if field_value != "":
               embed.insert_field_at(field_amt, name="Permissions", value=field_value, inline=False)
            await context.send(embed=embed)
        else:
            filteredDesc = []
            for item in theoristPerms:
                if filter in item:
                    filteredDesc.append(item)
            if filteredDesc != []:
                for entry in filteredDesc:
                    if len(field_value) + len(entry) >= 1024:
                        embed.insert_field_at(0, name="Permissions", value=field_value)
                        field_value = ""
                    field_value += entry
                if field_value != "":
                    embed.insert_field_at(0, name="Permissions", value=field_value)
                await context.send(embed=embed)
            else:
                await context.send("There are no leveled permissions corresponding to that filter.")



def setup(bot):
    bot.add_cog(UtilitiesPlugin(bot))
