"""This module provides all the functionality for the `ModContactPlugin`.

Attributes
----------
SELECT_CONTACT: str
    Used for an `aiopg` execution. Returns the 24 most recent open Mod Contacts for the related user.
VIEW_CONTACT: str
    Used for an `aiopg` execution. Returns a single discord_user_discordmodcontact record and all related responses.
NEW_CONTACT: str
    Used for an `aiopg` execution. Adds a new record to discord_user_discordmodcontact.
MOD_SELECT_CONTACT: str
    Used for an `aiopg` execution. Returns the all open Mod Contacts for the related user.
MOD_REPLY_CONTACT: str
    Used for an `aiopg` execution. Returns the a Mod Contact and all Replies for one Contact.
CLOSE_CONTACT: str
    Used for an `aiopg` execution. Updates a record to be marked as `closed`.
INSERT_RESPONSE: str
    Used for an `aiopg` execution. Inserts a "reply" into the Database.
TIMEOUT: double
    The module-wide timeout for async/wait_for functions.
"""
import datetime
import asyncio

import discord
from discord.ext import commands
from discord.utils import get
from discord.utils import find
from discord import Embed
from discord.ext.commands import has_any_role
from settings.base import COLOR

from plugins.base import BasePlugin


TIMEOUT = 30.0

SELECT_CONTACT = 'SELECT c.id, c.date_recieved, c.reason, c.urgency, c.contact_back, c.discord_user_id, COUNT(r.id) responses, c.closed  FROM discord_user_discordmodcontact AS c LEFT OUTER JOIN discord_user_discordmodcontactresponses AS r ON r.contact_id=c.id WHERE c.discord_user_id=%s AND c.closed=\'f\' GROUP BY c.id ORDER BY c.closed ASC LIMIT 24;'
VIEW_CONTACT = "SELECT * FROM discord_user_discordmodcontactresponses AS r RIGHT JOIN discord_user_discordmodcontact AS mc ON mc.id=r.contact_id WHERE mc.id=%s"
NEW_CONTACT = "INSERT INTO discord_user_discordmodcontact(date_recieved, reason, urgency, contact_back, discord_user_id, closed) VALUES(%s, %s, %s, %s, %s, 'f') RETURNING id;"
MOD_SELECT_CONTACT = 'SELECT c.id, c.date_recieved, c.reason, c.urgency, c.contact_back, c.discord_user_id, COUNT(r.id) responses, c.closed, u.discord_user_id  FROM discord_user_discordmodcontact AS c LEFT OUTER JOIN discord_user_discordmodcontactresponses AS r ON r.contact_id=c.id LEFT OUTER JOIN discord_user_discorduser AS u ON u.id=c.discord_user_id WHERE c.closed=\'f\' GROUP BY c.id,u.discord_user_id ORDER BY c.closed ASC;'
MOD_REPLY_CONTACT = 'SELECT c.id, c.date_recieved, c.reason, c.urgency, c.contact_back, c.discord_user_id, COUNT(r.id) responses, c.closed, u.discord_user_id  FROM discord_user_discordmodcontact AS c LEFT OUTER JOIN discord_user_discordmodcontactresponses AS r ON r.contact_id=c.id LEFT OUTER JOIN discord_user_discorduser AS u ON u.id=c.discord_user_id WHERE c.closed=\'f\' AND c.id=%s GROUP BY c.id,u.discord_user_id ORDER BY c.closed ASC;'
CLOSE_CONTACT = 'UPDATE discord_user_discordmodcontact SET closed=\'t\' WHERE id=%s;'
INSERT_RESPONSE = 'INSERT INTO discord_user_discordmodcontactresponses(date, message, mod_response, contact_id) VALUES (%s, %s, %s, %s)'

class ModContactPlugin(BasePlugin):
    """Plugin to handle group communication between Mods and other users.

    Note
    ----
    This module will only function if you have a running Postgres installation
    and the gtdiscord.discord_user_discorduser* tables imported! If you need this
    check the `sql/` folder!
    """
    def __init__(self, bot):
        """Basic plugin init

        Parameters
        ----------
        bot: discord.ext.commands.Bot
        """
        self.bot = bot

    async def _get_guild(self, context):
        """Private method that returns a guild

        This method will first check the provided `context.guild` for a `discord.Guild` object. If
        one cannot be found (such as the user is in a DMChannel), the user will be prompted to
        provide a `discord.Guild.id`. The object is then lookedup and returned.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.

        Returns
        -------
            guild: discord.Guild if successful or None
                If the user provides a `discord.Guild.id` to a guild the bot is in, the Guild is returned,
                otherwise None is returned.

        """
        if context.guild:
            return context.guild

        def check_is_dm(message):
            return context.message.author == message.author and isinstance(context.message.channel, discord.DMChannel)

        try:
            await context.send("Please send a Server ID for this Mod Contact:")
            message = await self.bot.wait_for('message', timeout=TIMEOUT, check=check_is_dm)
            guild = self.bot.get_guild(int(message.content))
            return guild
        except asyncio.TimeoutError as e:
            await context.send(":bettereyes:")
        except ValueError as e:
            await context.send("I need a Server ID to lookup, a number.")


    async def _build_single_embed_contact(self, context, guild, embed_data):
        """Private helper function to build a single embed.
        
        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.
        embed_data: dict
            A dictionary of all the embed data to use.

        Returns
        -------
        discord.Embed
            A formatted User Report Embed.
        """
        member = discord.utils.get(guild.members, id=embed_data[8])

        embed_title = "{username} Report - {date}".format(username=member.name,
                                                    date=embed_data[1].strftime("%B %d %Y"))
        desc = embed_data[2]

        embed = Embed(title=embed_title, description=desc, color=COLOR)
        embed.add_field(name="Urgency:", value=embed_data[3])
        embed.add_field(name="Contact Back:", value=embed_data[4])
        embed.add_field(name="Replies:", value=embed_data[6])

        return embed


    @commands.command(description="DM's a user instructions to start the mod contact",
                      case_insensitive=True,
                      aliases=['dm',])
    @has_any_role()
    async def dm_contact(self, context, user: discord.User.mention):
        pass


    @commands.group(description="Mod reports for this server.",
                      case_insensitive=True)
    async def mod_contact(self, context):
        """Placeholder for the ModContact Group command.
        """
        if context.invoked_subcommand is None:
            await context.send("What do you want to do with the Mod Contact?")


    @mod_contact.command(description="List all new Mod Contact Report for this server. (Mods)",
                      case_insensitive=True,
                      aliases=['l',])
    @has_any_role('Mods')
    async def list(self, context):
        """Command Method to display all open Mod Contact reports for Mods.

        Note
        ----
        Sends a `discord.Embed` with all open Mod Contact reports to the channel requested.

        - Uses `MOD_SELECT_CONTACT`
        """
        guild = context.guild
        async with context.channel.typing():
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(MOD_SELECT_CONTACT)
                    all_contacts = await cursor.fetchall()

        embed = Embed(title="All Contacts", color=COLOR)
        
        for contact in all_contacts:
            member = get(guild.members, id=contact[8])
            fmt_date = contact[1].strftime("%B %d %Y")
            field_name = "{username} - {date}".format(
                    username=member.name,  date=fmt_date)
            field_value = "*ContactID:* {contact_id}\n*Discord ID:* {discord_id}\n*Urgency:* {urgency}\n*Request Contact:* {req_contact}\n*Replies*: {replies}\n*Reason:* {reason}".format(
                    contact_id=contact[0], discord_id=contact[8], req_contact=contact[4], urgency=contact[3], reason=contact[2], replies=contact[6])
            embed.add_field(name=field_name, value=field_value, inline=False)

        await context.send(embed=embed)

    @mod_contact.command(description="Reply to a Mod Contact Report. (Mod)",
                      case_insensitive=True,
                      aliases=['r',])
    async def reply(self, context, contact_id, *reply):
        """Used to reply to a users Open Mod Contact.

        Parameters
        ----------
        contact_id: int
            The ID for the Mod Contact Record to reply to.
        *reply: list(str)
            A list of words to use as the Reply.   

        Note
        ----
        - Uses _build_single_embed_contact
        """
        now = datetime.datetime.now()
        member = context.message.author
        reply = " ".join(reply)
        guild = await self._get_guild(context)
        reply_output = "**{date}**\n{reason}".format(
                date=now.strftime("%B %d %Y"), reason=reply)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(MOD_REPLY_CONTACT, (contact_id, ))
                contact_data = await cursor.fetchone()
        contact_member = discord.utils.get(guild.members, id=contact_data[8])

        """Checks if the user is a moderator and if the user can Reply."""
        is_mod = False
        can_reply = False
        
        # is the Mod Contact owner and the Caller the same?
        if contact_member.id == member.id:
            can_reply = True
        # is the Caller a Mod?
        elif any('Mods' == role.name for role in member.roles):
            can_reply = True
            is_mod = True
        # Safety validation, checks that contact_id is a number
        if not contact_id and contact_id.isdigit():
            await context.send("Please send a contact id (number).")
            return

        """ Check if Mod or Contact Author """
        if not can_reply:
            await context.send("You cannot reply to this Mod Contact.")
            return

        contact_reply_embed = await self._build_single_embed_contact(context, guild, contact_data)
        await context.send(embed=contact_reply_embed)
        await context.send("The following reply will be added to Contact {}".format(contact_id))
        await context.send(reply_output)
        await context.send("Is this alright? React with :thumbsup: to send :x: to cancel.")

        def is_contact_and_dm_or_mod(emoji, user):
            """on_reaction_add check method to ensure Reactor is Caller and is in DMChannel"""
            return (member == user) and (isinstance(emoji.message.channel, discord.DMChannel) or any(role.name.lower() == "mods" for role in member.roles))

        try:
            # Setup seperate checks
            reaction, reaction_user = await self.bot.wait_for('reaction_add', timeout=TIMEOUT, check=is_contact_and_dm_or_mod)
            if reaction.emoji == "👍":
                # If everything is correct, save to DB
                async with context.channel.typing():
                    async with self.bot.pg_pool.acquire() as conn:
                        async with conn.cursor() as cursor:
                            await cursor.execute(INSERT_RESPONSE, (datetime.datetime.now(), reply, is_mod, contact_id))
                            await context.send("Reply added!")
            elif reaction.emoji == "❌":
                # Canceling requires nothing!
                await context.send("Okay! Canceling!")
            else:
                await context.send("Sorry! Only the author of that contact or a mod can reply!")
        except asyncio.TimeoutError as e:
            await context.send(":bettereyes:")

        if contact_member.id == member.id:
            mod_channel = get(guild.text_channels, name="mod-contacts")
            await mod_channel.send("New Mod Contact Reply on ID: {}".format(contact_id))
        else:
            await contact_member.send("New reply for your Mod Contact ID: {}! \n\n{}".format(contact_id, reply))

    @mod_contact.command(description="Close a Mod Contact Report. (Mod)",
                      case_insensitive=True,
                      aliases=['c',])
    @has_any_role('Mods')
    async def close(self, context, contact_id=None):
        """Closes a Mod Contact.
        
        Parameters
        ----------
        contact_id: int or None
            The id for a discord_user_discordmodcontact record.

        Note
        ----
        - Uses `CLOSE_CONTACT`
        """
        guild = context.guild

        # Saftey validation that the contact_id is a number
        if not contact_id and contact_id.isdigit():
            await context.send("Please provide a contact id (number).")
            return

        async with context.channel.typing():
            async with self.bot.pg_pool.acquire() as conn:
                async with conn.cursor() as cursor:
                    await cursor.execute(CLOSE_CONTACT, (contact_id,))
                    await context.send("{} has been closed!".format(contact_id))

    async def _build_open_embed(self, member, guild, link_id):
        """Protected method for building a single embed with all of the users `open` contacts

        Returns
        -------
        discord.Embed
            An Embed with fields representing all of the users open contacts."""
        # Create the Interactive Embed
        embed_title = "Mod Contact"
        description = (
            " Welcome! This embed displays all Mod Contact Reports from {author} to "
            "the Mod Team on {server_name}. You can find a list of all submitted reports below"
        )
        description = description.format(
                author=member.name,
                server_name=guild.name)

        embed = Embed(title=embed_title, description=description, color=COLOR)
        embed.set_author(name=member.name, icon_url=member.avatar_url)

        # Get all mod contacts for this user
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(SELECT_CONTACT, (link_id, ))
                contacts = await cursor.fetchall()

        # Attach each of the callers Mod Contacts to embed as an overview
        if contacts:
            for contact in contacts:
                is_open = "Closed" if bool(contact[7]) else "Open"
                field_name = "{is_open} - ID : {cid} - {date}".format(is_open=is_open, cid=contact[0], date=contact[1].strftime("%B %d %Y"))
                field_val = "*Urgency:* {urg}\n*Requested Contact Back:* {contact_back}\n*Replies:* {replies}\n*Reason:* {reason}".format(
                        urg=contact[3], contact_back=contact[4], replies=contact[6], reason=contact[2])
                embed.add_field(name=field_name, value=field_val, inline=False)

        # Set the embed footer
        embed.set_footer(text="React with ➕ to submit a new report, 💬 to reply or view mod replies, or ❌ to close.")
        return embed

    @mod_contact.command(description="Start a new Mod Contact Report for this server.",
                      case_insensitive=True,
                      aliases=['o',])
    async def open(self, context):
        """Displays a list of all Open Mod Contacts for the Caller and starts a new Contact

        Note
        ----
        - Uses _check_user_exists
        - Uses SELECT_CONTACT
        - Uses _new_mod_contact
        """
        member = context.message.author
        guild = await self._get_guild(context)
        link_id = await self._check_user_exists(member.id, guild.id)

        embed = await self._build_open_embed(member, guild, link_id)

        await member.send(embed=embed)

        def check_is_dm(reaction, user):
            """on_reaction_add check to ensure the user is in a DMChannel"""
            return member == user and isinstance(reaction.message.channel, discord.DMChannel)

        while True:
            try:
                reaction, reaction_user = await self.bot.wait_for('reaction_add', timeout=TIMEOUT, check=check_is_dm)
                if reaction.emoji == "➕":
                    await self._new_mod_contact(context, guild, link_id)
                    break
                elif reaction.emoji == "💬":
                    await self._view_mod_contact(context, guild, link_id)
                    break
                elif reaction.emoji == "❌":
                    await member.send("Canceling...")
                    break
            except asyncio.TimeoutError as e:
                await member.send(":bettereyes:")

    async def _view_mod_contact(self, context, guild, link_id):
        """Private method that displays all replies for a specific Mod Report.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.
        guild: discord.Guild
            The guild this mod_contact will be associated with.
        link_id: str
            The id of this users discord_user_discorduser record.
        """
        member = context.message.author

        def is_contact_and_dm_or_mod(message):
            """on_reaction_add check method to ensure Reactor is Caller and is in DMChannel"""
            return (member == message.author) and (isinstance(message.channel, discord.DMChannel) or any(role.name.lower() == "mods" for role in member.roles))

        while True:
            try:
                await member.send("What contact do you want to view? Reply with an ID.")
                contact_id = await self.bot.wait_for('message', timeout=TIMEOUT, check=is_contact_and_dm_or_mod)
            except asyncio.TimeoutError as e:
                await member.send(":bettereyes:")
            
            if contact_id.content.isdigit():
                contact_id = int(contact_id.content)
                break

        await self._view_single_contact(contact_id, guild, member)

    @mod_contact.command(description="View mod contact replies",
                      case_insensitive=True,
                      aliases=['v',])
    @has_any_role("Mods")
    async def view(self, context, contact_id: int):
        await self._view_single_contact(contact_id, context.guild, context.channel)

    async def _view_single_contact(self, contact_id, guild, send_to):
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                # Creates the new record
                await cursor.execute(VIEW_CONTACT, (contact_id,))
                contacts = await cursor.fetchall()
                await cursor.execute("SELECT * FROM discord_user_discorduser WHERE id=%s", (contacts[0][10], ))
                member_id = await cursor.fetchone()
        
        contact_author = self.bot.get_user(member_id[2])
        is_open = "Closed" if bool(contacts[0][11]) else "Open"
        fmt_date = contacts[0][6].strftime("%B %d %Y")
        title = "{} - ID: {} - {}".format(is_open, contacts[0][5], fmt_date)

        embed = Embed(title=title, description=contacts[0][7], color=COLOR)
        embed.set_author(name=contact_author.name, icon_url=contact_author.avatar_url)

    
        if contacts[0][1]:
            for contact in contacts:
                reply_author = "Mod" if bool(contact[3]) else contact_author.name
                reply_date = contact[1].strftime("%B %d %Y")
                field_name = "{} replied on {}".format(reply_author, reply_date)
                embed.add_field(name=field_name, value=contact[2], inline=False)

        embed.set_footer(text="You can reply with `v!mod_contact reply {} Your reply here` | Guild ID: {}".format(contacts[0][5], guild.id))

        await send_to.send(embed=embed)


    async def _new_mod_contact(self, context, guild, link_id):
        """Private method that walks the Caller through submiting a Mod Report.

        This method has three major sections. 

        The first section is wrapped in a `try` block and contains the logic
        the bot uses to collect the Callers report. Each question is self contained
        in a `while True:` loop. These loops contain the validation for each question
        and a break condition.

        The second section build an embed with the new information, and displays the embed
        to the Caller for confirmation.

        Once confirmed the bot saves this new report using `NEW_CONTACT` and posts the confirmation
        embed to a #mod-contact channel on the appropriate guild.

        Parameters
        ----------
        context: discord.Context
            A discordpy Context object.

        guild: discord.Guild
            The Guild that this report is for.

        link_id: int
            The discord_user_discorduser id for the Callers record.

        Note
        ----
        - Uses `NEW_CONTACT`
        """
        member = context.message.author

        def check_is_dm(message):
            """Ensures the Caller is the Mod Contact author and is in a DMChannel"""
            return member == message.author and isinstance(message.channel, discord.DMChannel)

        await member.send("OK! We take all reports very seriously! Let me start a new report now. I will just need to ask a few questions...")
        
        try:
            while True:
                # determines how "urgent" the issue is and validates the reply is a number
                await member.send("On a scale of 1 to 7, how urgent is this issue (1 - Not urgent, 7 - Incredibly urgent)")
                urgency = await self.bot.wait_for('message', timeout=TIMEOUT, check=check_is_dm)

                try:
                    urgency = int(urgency.content)
                    break
                except ValueError as e:
                    await member.send("Please provide a value between 1 - 7")
            
            # Collects the Callers response
            await member.send("Please tell us about your issue in detail. If you would like to supply screenshots or images, please use Imgur or other filesharing platforms and provide URLs.")
            reason = await self.bot.wait_for('message', timeout=TIMEOUT, check=check_is_dm)

            while True:
                # determines if the caller would like contacted back and validates the reply is a number
                await member.send("Would you like a mod to contact you? (y/n)")
                contact = await self.bot.wait_for('message', timeout=TIMEOUT, check=check_is_dm)
                if contact.content.lower() in ["y", "yes", "ye",]:
                    contact = True
                    break
                elif contact.content.lower() in ["n", "no",]:
                    contact = False
                    break
                else:
                    await member.send("Please answer 'yes' or 'no'.")

        except asyncio.TimeoutError as e:
            await context.send(":bettereyes:")
        

        # Builds and sends the confimration embed
        desc = "Please take a moment to review your contact! This will be submitted to the Server when you're finished."
        embed = Embed(title="New Mod Contact", description=desc, color=COLOR)
        embed.add_field(name="Urgency:", value=urgency)
        embed.add_field(name="Contact Back:", value=contact)
        embed.add_field(name="Reporter:", value="{} - ID: {}".format(member.name, member.id))
        embed.add_field(name="Reason:", value=reason.content, inline=False)
        embed.set_author(name=member.name, icon_url=member.avatar_url)
        embed.set_footer(text="If this is correct, react with 👍 to submit. Otherwise, ❌ to close and clear.")

        await member.send(embed=embed)

        try:
            def check_is_dm_react(reaction, user):
                """Ensures the user is in a DMChannel"""
                return context.message.author == user and isinstance(reaction.message.channel, discord.DMChannel)

            reaction, reaction_user = await self.bot.wait_for('reaction_add', timeout=TIMEOUT, check=check_is_dm_react)
            if reaction.emoji == "👍":
                async with self.bot.pg_pool.acquire() as conn:
                    async with conn.cursor() as cursor:
                        # Creates the new record
                        await cursor.execute(NEW_CONTACT, (datetime.datetime.now(), reason.content, urgency, contact, link_id))
                        await cursor.execute(SELECT_CONTACT, (link_id,))
                        mid = await cursor.fetchall()
                        mid = mid[-1][0]
                        await member.send("Thanks for the mod contact! Your contact id is {}".format(mid))
            elif reaction.emoji  == "❌":
                await context.send("Sorry to hear that. If you have any other issues, please report them right away!")
        except asyncio.TimeoutError as e:
            await member.send(":bettereyes:")
        
        desc = "You recieved a new Report!"
        # Sends the confirmation embed to the #mod-contacts channel and pins
        mod_channel = get(guild.text_channels, name="mod-contacts")
        embed.add_field(name="Contact ID:", value=mid, inline=False)
        mod_message = await mod_channel.send(embed=embed)
        await mod_message.pin()


def setup(bot):
    """Setup function for Cogs"""
    bot.add_cog(ModContactPlugin(bot))
