import discord
from discord import Embed
from discord.ext import commands
from discord.utils import get
from discord.ext.commands import has_any_role

from .base import BasePlugin

CHAN1_NAME = "among-us"
CHAN2_NAME = "among-them"

class AmongUsPlugin(BasePlugin):
    def __init__(self, bot):
        self.bot = bot
        self.chan1_id = 760322963391119400 #needs to be changed to whatever GTD's channel id is, this ones for testing
        self.host1 = None
        self.players1 = 0
        self.chan2_id = 760323000305713202 #needs to be changed to whatever GTD's channel id is, this ones for testing
        self.host2 = None
        self.players2 = 0

    def check_for_role(*roles):
        #Helper function that returns true if the user has none of the roles in the list, used for no roles
        async def predicate(context):
            if all(discord.utils.get(context.guild.roles, name=role) not in context.author.roles for role in roles):
                return True
            await context.send("You can't do Among Us commands because you have the No Among Us role. :(")
        return commands.check(predicate)

    @commands.group(description="Among Us",
                    case_insensitive=True,
                    aliases=['au',])
    @check_for_role("No Among Us")
    async def among_us(self, context):
        if context.invoked_subcommand is None:
            await context.send('Please run a subcommand!')

    @among_us.command(description="test command", case_insensitive=True)
    @check_for_role("No Among Us")
    async def test(self, context):
        await context.send("Test successful. Plugin online.")

    @among_us.command(description="claim a room to host", case_insensitive=True)
    async def host(self, context, claimNum=None):
        role1 = discord.utils.get(context.guild.roles, name='Among Us Game 1')
        role2 = discord.utils.get(context.guild.roles, name='Among Us Game 2')
        if claimNum == "1" and self.host1 == None and not role2 in context.author.roles:
            await context.send("{} has claimed Among Us Game 1 to host.".format(context.author.mention))
            self.host1 = context.author
            await context.author.add_roles(role1)
            self.players1 += 1

        elif claimNum == "2" and self.host2 == None and not role1 in context.author.roles:
            await context.send("{} has claimed Among Us Game 2 to host.".format(context.author.mention))
            self.host2 = context.author
            await context.author.add_roles(role2)
            self.players2 += 1

        else: 
            await context.send("You must pick an empty room to host, either room 1 or room 2. You also can't host both rooms.")
    
    @among_us.command(description="show active rooms", case_insensitive=True)
    async def active(self, context):
        if self.host1 == None:
            host1 = "None"
        else:
            host1 = self.host1.mention
        if self.host2 == None:
            host2 = "None"
        else:
            host2 = self.host2.mention

        embed = discord.Embed(title="Current Active Among Us Games", color=context.author.top_role.color)
        embed.add_field(name="Game 1", value="Host: {host1}\nPlayers: {players1}".format(host1=host1, players1=self.players1), inline=False)
        embed.add_field(name="Game 2", value="Host: {host2}\nPlayers: {players2}".format(host2=host2, players2=self.players2), inline=False)
        await context.send(embed=embed)

    @among_us.command(description="join a room", case_insensitive=True)
    async def join(self, context):
        role1 = discord.utils.get(context.guild.roles, name='Among Us Game 1')
        role2 = discord.utils.get(context.guild.roles, name='Among Us Game 2')
        chan1 = get(context.guild.text_channels, name=CHAN1_NAME)
        chan2 = get(context.guild.text_channels, name=CHAN2_NAME)
        #chan1 = context.guild.get_channel(self.chan1_id)
        #chan2 = context.guild.get_channel(self.chan2_id)
        if context.message.mentions:
            if self.host1 != None and context.message.mentions[0] == self.host1:
                if self.players1 <= 10:
                    await context.author.add_roles(role1)
                    self.players1 += 1
                    await chan1.send("{host}, {player} has joined your lobby.".format(host=self.host1.mention,player=context.author.mention))
                else:
                    await context.send("There is a limit of 10 players to a game. Try again later.")
            elif self.host2 != None and context.message.mentions[0] == self.host2:
                if self.players2 <= 10:
                    await context.author.add_roles(role2)
                    self.players2 += 1
                    await chan2.send("{host}, {player} has joined your lobby.".format(host=self.host2.mention,player=context.author.mention))
                else:
                    await context.send("There is a limit of 10 players to a game. Try again later.")
            else:
                await context.send("You must ping the host of a room.")
        else:
            await context.send("You must ping the host of a room.")

    @among_us.command(description="leave a room", case_insensitive=True)
    @has_any_role("Among Us Game 1", "Among Us Game 2")
    async def leave(self, context):
        role1 = discord.utils.get(context.guild.roles, name='Among Us Game 1')
        role2 = discord.utils.get(context.guild.roles, name='Among Us Game 2')
        chan1 = get(context.guild.text_channels, name=CHAN1_NAME)
        chan2 = get(context.guild.text_channels, name=CHAN2_NAME)
        #chan1 = context.guild.get_channel(self.chan1_id)
        #chan2 = context.guild.get_channel(self.chan2_id)

        if context.author == self.host1:
            if self.players1 == 1:
                self.host1 = None
            elif context.message.mentions and role1 in context.message.mentions[0].roles:
                self.host1 = context.message.mentions[0]
                await chan1.send("{player} has left game 1, {host} is now the host.".format(player=context.author.mention, host=context.message.mentions[0].mention))
            else:
                await context.send("Ping someone in the game after ve!au leave to bequeath your hostship to.")
                return
        if context.author == self.host2:
            if self.players2 == 1:
                self.host2 = None
            elif context.message.mentions and role2 in context.message.mentions[0].roles:
                self.host2 = context.message.mentions[0]
                await chan2.send("{player} has left game 2, {host} is now the host.".format(player=context.author.mention, host=context.message.mentions[0].mention))
            else:
                await context.send("Ping someone in the game after ve!au leave to bequeath your hostship to.")
                return
        if role1 in context.author.roles:
            self.players1 -= 1
            await chan1.send("{} has left game 1.".format(context.author.mention))
            await context.author.remove_roles(role1)
        elif role2 in context.author.roles:
            self.players2 -= 1
            await chan2.send("{} has left game 2.".format(context.author.mention))
            await context.author.remove_roles(role2)
        else:
            await context.send("You can't leave a room unless you're in one first, right?")
        
    
    @among_us.command(description="kick someone from a room", case_insensitive=True)
    @has_any_role("Server Glue", "Disaster Director", "Mods")
    async def kick(self, context):
        role1 = discord.utils.get(context.guild.roles, name='Among Us Game 1')
        role2 = discord.utils.get(context.guild.roles, name='Among Us Game 2')
        if context.message.mentions:
            if role1 in context.message.mentions[0].roles:
                if context.message.mentions[0] == self.host1:
                    if len(context.message.mentions) == 2:
                        if role1 in context.message.mentions[1].roles:
                            await chan1.send("Hostship has been moved to {host}, {kicked} has been kicked.".format(host=context.message.mentions[1].mention, kicked=context.message.mentions[0].mention))
                            self.host1 = context.message.mentions[1]
                            return
                        else:
                            await context.send("You have to ping a person in your room to give hostship to.")
                    else:
                        await context.send("You have to ping a person to give hostship to after the kicked person.")
                        return
                await context.send("{kicked} has been kicked.".format(kicked=context.message.mentions[0].mention))
                await context.message.mentions[0].remove_roles(role1)
                self.players1 -= 1
            elif role2 in context.message.mentions[0].roles:
                if context.message.mentions[0] == self.host2:
                    if len(context.message.mentions) == 2:
                        if role2 in context.message.mentions[1].roles:
                            await chan2.send("Hostship has been moved to {host}, {kicked} has been kicked.".format(host=context.message.mentions[1].mention, kicked=context.message.mentions[0].mention))
                            self.host2 = context.message.mentions[1]
                            return
                        else:
                            await context.send("You have to ping a person in your room to give hostship to.")
                    else:
                        await context.send("You have to ping a person to give hostship to after the kicked person.")
                        return
                await context.send("{kicked} has been kicked.".format(kicked=context.message.mentions[0].mention))
                await context.message.mentions[0].remove_roles(role2)
                self.players2 -= 1
            else:
                await context.send("The person you tried to kick isn't currently playing among us.")
        else:
            await context.send("You have to ping someone to kick.")

    @among_us.command(description="show hosts of the rooms", case_insensitive=True)
    async def show_hosts(self, context):
        await context.send("self.host1 = {host1}\nself.host2 = {host2}".format(host1=self.host1, host2=self.host2))




def setup(bot):
    bot.add_cog(AmongUsPlugin(bot))
