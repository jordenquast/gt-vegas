from discord.ext import commands
from discord.ext import tasks
from discord.utils import get
from discord.ext.commands import has_any_role

from discord import Embed

from settings.local_settings import GUILD_ID
from settings.base import COLOR


class DeleterPlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.autodeleter_loop.start()

    @tasks.loop(minutes=5.0)
    async def autodeleter_loop(self):
        await self.bot.wait_until_ready()
        guild = self.bot.get_guild(GUILD_ID)

        def is_pinned(message):
            return not message.pinned

        bot_spam_chan = get(guild.text_channels, name='bot-spam')
        if bot_spam_chan:
            deleted = await bot_spam_chan.purge(limit=300, check=is_pinned, bulk=False)

        get_access_chan = get(guild.text_channels, name='get-access')
        if get_access_chan:
            deleted = await get_access_chan.purge(limit=300, check=is_pinned, bulk=True)

    @commands.command(name='delete',
            description='Deletes so many messages',
            aliases=['d', 'clear', 'del',])
    @has_any_role('Can Delete')
    async def delete(self, context, to_del=50, keep_pins=True):
        def is_pinned(message):
            if not bool(keep_pins):
                return True
            return not message.pinned

        deleted = await context.message.channel.purge(limit=to_del, check=is_pinned, bulk=True)
        embed = Embed(title="Deleted {} messages!".format(len(deleted)), color=COLOR)
        confirm = await context.send(embed=embed)
        await confirm.delete(delay=5)

def setup(bot):
    bot.add_cog(DeleterPlugin(bot))
