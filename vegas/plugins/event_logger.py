from textwrap import wrap

import discord
from discord.utils import get
from discord.ext import commands

from settings.local_settings import IN_NICK
from settings.local_settings import WORRY_CHECK

import datetime

from settings.local_settings import GUILD_ID


LOG_CHANNEL_NAME = 'vegas-logging'
SPAM_LOG_CHANNEL_NAME = 'bot-spam-logging'

IGNORED_CHANNELS = ['the-senate', 'the-senate-bot-spam',]


COLORS = {
        'ban': 7821292,
        'unban': 13429847,
        'edit': 15538157,
        'delete': 14487859,
        'voice': 1904757,
        'role_removed': 16759569,
        'role_added': 4513245,
        'join': 15663086,
        'left': 1114129,
        'profile_change': 14318337,
        'username': 14318337,
        'avatar': 14318337,
    }


class EventLoggerPlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def event_logging_channel(self):
        server = self.bot.get_guild(GUILD_ID)
        return get(server.text_channels, name=LOG_CHANNEL_NAME)

    def spam_logging_channel(self):
        server = self.bot.get_guild(GUILD_ID)
        return get(server.text_channels, name=LOG_CHANNEL_NAME)

    def mods_ping(self, roles):
        mod_role =  get(roles, name='Mods')
        return mod_role

    def dds_ping(self, roles):
        dd_role = get(roles, name='Disaster Director')
        return dd_role

    @commands.Cog.listener()
    async def on_member_update(self, before:discord.Member, after:discord.Member):
        had_before = None
        had_after = None
        embed_color = COLORS['profile_change']
        membername = "{membername}#{disc}".format(
                membername=before.name,
                disc=before.discriminator)
        embed_footer = "Author ID: {aid}".format(
                aid=before.id)
        roles = before.guild.roles
        mods_ping = self.mods_ping(roles)
        mods_ids = [member.id for member in mods_ping.members]
        dds_ping = self.dds_ping(roles)
        if dds_ping:
            dds_ids = [member.id for member in dds_ping.members]

        if before.roles != after.roles:
            before_set = set(before.roles)
            after_set = set(after.roles)
            had_before = before_set - after_set
            had_after = after_set - before_set
            log_format = "{}  ID: {}"
            if had_before:
                embed_color = COLORS['role_removed']
            elif had_after:
                embed_color = COLORS['role_added']


        embed = discord.Embed(description="Member Updated Updated", color=embed_color)
        embed.set_thumbnail(url=before.avatar_url)
        embed.set_author(name=membername, icon_url=before.avatar_url)
        embed.set_footer(text=embed_footer)
        if before.nick != after.nick:
            before_nick = before.nick if before.nick != '' else '[None]'
            after_nick = after.nick if after.nick != '' else '[None]'

            if any(nick_catch in after.nick.lower() for nick_catch in IN_NICK):
                if after.id not in mods_ids or after.id not in dds_ids:
                    await self.event_logging_channel().send(
                            "Suspicious Nickname {}".format(mods_ping.mention)
                    )

            if any(nick_catch in after.nick.lower() for nick_catch in WORRY_CHECK):
                if after.id not in mods_ids or after.id not in dds_ids:
                    watching = get(before.guild.roles, name="Watching")
                    await self.event_logging_channel().send(
                            "Suspicious Nickname {} <{}> banned - {}".format(after.name, after.id, watching.mention)
                    )
                    breason = "You have been banned on alt suspicion. If this was incorrect please fill out this form: https://bit.ly/GTD-Ban-Appeal"
                    await after.send(breason)
                    await after.ban(reason=breason)

            embed.add_field(name="Nick Before:", value=before_nick, inline=False)
            embed.add_field(name="Nick After:", value=after_nick, inline=False)

        if had_before:
            before_out = '\n'.join(map(lambda x: log_format.format(x.name, x.id), had_before))
            embed.add_field(name="Removed Roles:", value=before_out, inline=False)
        if had_after:
            after_out = '\n'.join(map(lambda x: log_format.format(x.name, x.id), had_after))
            embed.add_field(name="Added Roles:", value=after_out, inline=False)

        if len(embed.fields):
            await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_user_update(self, before:discord.Member, after:discord.Member):
        title = "Memeber Account {} Updated"
        color = COLORS['join']
        before_field = None
        after_field = None
        use_icon = False
        '''
        if after.avatar != before.avatar:
            title = title.format("Avatar")
            color = COLORS['avatar']
            before_field = before.avatar_url
            after_field = after.avatar_url
            use_icon = True
        '''
        if before.name != after.name:
            title = title.format("Username")
            color = COLORS['username']
            before_field = before.name
            after_field = after.name

        if before_field:
            embed_description = after.mention
            embed = discord.Embed(description=embed_description, title=title, color=color)
            embed.add_field(name="Before:", value=before_field, inline=False)
            embed.add_field(name="After:", value=after_field, inline=False)

            if use_icon:
                embed.set_thumbnail(url=after_field)

            await self.event_logging_channel().send(embed=embed)


    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        membername = "{membername}#{disc}".format(
                membername=member.name,
                disc=member.discriminator)
        embed_description = "{mention} {membername}".format(
                mention=member.mention,
                membername=membername)
        embed_footer = "ID: {aid}".format(
                aid=member.id)

        embed = discord.Embed(description=embed_description, color=COLORS['voice'])
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_author(name="Member VC Changed", icon_url=member.avatar_url)
        if before.mute is not after.mute:
            mute_status = "User Server Muted" if after.mute else "User Server Unmuted"
            embed.add_field(name="Mute", value=mute_status)
        if before.self_mute is not after.self_mute:
            self_mute_status = "User Self Muted" if after.self_mute else "User Removed Self Mute"
            embed.add_field(name="Self Mute", value=self_mute_status)
        if before.deaf is not after.deaf:
            deaf_status = "User Deafened" if after.deaf else "User UnDeafened"
            embed.add_field(name="deaf", value=deaf_status)
        if before.self_deaf is not after.self_deaf:
            self_deaf_status = "User Self Defened" if after.self_deaf else "User Removed Self Deafen"
            embed.add_field(name="Self Deafened", value=self_deaf_status)
        if before.self_stream is not after.self_stream:
            self_stream_status = "User Started Streaming" if after.self_stream else "User Ended a Stream"
            embed.add_field(name="Stream Status", value=self_stream_status)
        if before.self_video is not after.self_video:
            self_video_status = "User Started Broadcasting videoo" if after.self_video else "User Ended Broadcasting videoo"
            embed.add_field(name="Broadcast video", value=self_video_status)
        before_channel = "Joined VC "
        after_channel = " left VC"
        if before.channel is not None:
            before_channel = "Moved from {} ".format(before.channel.name)
        if before.channel is not after.channel:
            if after.channel is not None:
                after_channel = "to {}".format(after.channel.name)
            channel_status = before_channel + after_channel
            embed.add_field(name="Channel", value=channel_status)
        embed.set_footer(text=embed_footer)

        await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_member_join(self, member):
        t = datetime.datetime.now()
        c = member.created_at
        guild = self.bot.get_guild(GUILD_ID)
        roles = guild.roles
        outdiff = f"{t.year - c.year} years, {t.month - c.month} months, {t.day - c.day} days, {t.hour - c.hour} hours, {t.minute - c.minute} minutes, {t.second - c.second} seconds ago"

        membername = "{membername}#{disc}".format(
                membername=member.name,
                disc=member.discriminator)
        embed_description = "{mention} {membername}\nCreated {time}".format(
                mention=member.mention,
                membername=membername,
                time=outdiff)
        embed_footer = "ID: {aid} - Account Created: {acreated}".format(
                aid=member.id, acreated=member.created_at)

        mods_ping = self.mods_ping(roles)
        mods_ids = [member.id for member in mods_ping.members]
        dds_ping = self.dds_ping(roles)
        if dds_ping:
            dds_ids = [member.id for member in dds_ping.members]

        # Ping mods for suspicious nickname
        if any(nick_catch in member.name.lower() for nick_catch in IN_NICK):
            if member.id not in mods_ids or member.id not in dds_ids:
                await self.event_logging_channel().send(
                        "Suspicious Nickname {}".format(mods_ping.mention)
                )
        # Instantly ban users if they meet this criteria then ping watching
        if any(nick_catch in member.name.lower() for nick_catch in WORRY_CHECK):
            if member.id not in mods_ids or member.id not in dds_ids:
                watching = get(member.guild.roles, name="Watching")
                await self.event_logging_channel().send(
                        "Suspicious Nickname {} <{}> banned - {}".format(member.name, member.id, watching.mention)
                )
                breason = "You have been banned on alt suspicion. If this was incorrect please fill out this form: https://bit.ly/GTD-Ban-Appeal"
                await member.send(breason)
                await member.ban(reason=breason)

        embed = discord.Embed(description=embed_description, color=COLORS['join'])
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="Member Joined", icon_url=member.avatar_url)
        await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        membername = "{membername}#{disc}".format(
                membername=member.name,
                disc=member.discriminator)
        embed_description = "{mention} {membername}".format(
                mention=member.mention,
                membername=membername)
        embed_footer = "ID: {aid}".format(
                aid=member.id)

        no_roles = ''

        for role in member.roles:
            if "no " in role.name.lower():
                if no_roles == '':
                    no_roles = role.name + "\n"
                else:
                    no_roles += role.name + "\n"

        if no_roles == '':
            no_roles = '[None]'

        embed = discord.Embed(description=embed_description, color=COLORS['left'])
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="Member Left", icon_url=member.avatar_url)
        embed.add_field(name="No Roles:", value=no_roles)
        await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_member_ban(self, guild, user):
        try:
            await user.send("lmao thannks for playing! https://bit.ly/GTD-Ban-Appeal")
        except Exception:
            pass
        username = "{username}#{disc}".format(
                username=user.name,
                disc=user.discriminator)
        embed_description = "{mention} {username}".format(
                mention=user.mention,
                username=username)
        embed_footer = "ID: {aid}".format(
                aid=user.id)

        embed = discord.Embed(description=embed_description,color=COLORS['ban'])
        embed.set_thumbnail(url=user.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="lmao gottem", icon_url=user.avatar_url)
        await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_member_unban(self, guild, user):
        username = "{username}#{disc}".format(
                username=user.name,
                disc=user.discriminator)
        embed_description = "{mention} {username}".format(
                mention=user.mention,
                username=username)
        embed_footer = "ID: {aid}".format(
                aid=user.id)

        embed = discord.Embed(description=embed_description,color=COLORS['unban'])
        embed.set_thumbnail(url=user.avatar_url)
        embed.set_footer(text=embed_footer)
        embed.set_author(name="welcome back", icon_url=user.avatar_url)
        await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_bulk_message_delete(self, messages):
        if not isinstance(messages[0].channel, discord.DMChannel):
            embed_description = "Bulk delete in {channel}, {x} messages deleted".format(
                    channel=messages[0].channel.mention,
                    x=len(messages))

            embed = discord.Embed(description=embed_description,color=COLORS['delete'])
            if messages[0].channel.name == 'bot-spam':
                await self.spam_logging_channel(messages[0].guild.text_channels).send(embed=embed)
            else:
                await self.event_logging_channel(messages[0].guild.text_channels).send(embed=embed)

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        if message.channel.name not in IGNORED_CHANNELS and not isinstance(message.channel, discord.DMChannel):
            message.content = message.content if message.content != '' else '[Empty]'
            m = message.content
            embed_description = "Message sent by {user} deleted in {channel}".format(
                    user=message.author.mention,
                    channel=message.channel.mention)
            embed_footer = "Author ID: {aid} | Message ID: {mid}".format(
                    aid=message.author.id,
                    mid=message.id)
            username = "{username}#{disc}".format(
                    username=message.author.name,
                    disc=message.author.discriminator)
            embed = discord.Embed(description=embed_description,color=COLORS['delete'])
            embed.set_thumbnail(url=message.author.avatar_url)
            embed.set_footer(text=embed_footer)
            embed.set_author(name=username, icon_url=message.author.avatar_url)

            message_content = wrap(message.content, 1000)

            for field_value in message_content:
                embed.add_field(name="After Content:", value=field_value, inline=False)

            if message.channel.name == 'bot-spam':
                await self.spam_logging_channel().send(embed=embed)
            else:
                await self.event_logging_channel().send(embed=embed)

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        if before.content != '' and after.content != '' and not isinstance(before.channel, discord.DMChannel):
            if before.channel.name not in IGNORED_CHANNELS:
                after.content = after.content if after.content != '' else '[Deleted]'
                before.content = before.content if before.content != '' else '[Deleted]'
                embed_description = "Message edited in {channel} \n\n [Jump to Message]({link})".format(
                        channel=before.channel.mention,
                        link=before.jump_url)
                embed_footer = "Author ID: {aid}".format(
                        aid=before.author.id)
                username = "{username}#{disc}".format(
                        username=before.author.name,
                        disc=before.author.discriminator)

                embed = discord.Embed(description=embed_description,color=COLORS['edit'])
                embed.set_thumbnail(url=before.author.avatar_url)
                embed.set_footer(text=embed_footer)
                embed.set_author(name=username, icon_url=before.author.avatar_url)
                
                before_content = wrap(before.content, 1000)

                for field_value in before_content:
                    embed.add_field(name="Before Content:", value=field_value, inline=False)

                after_content = wrap(after.content, 1000)

                for field_value in after_content:
                    embed.add_field(name="After Content:", value=field_value, inline=False)

                if before.channel.name == 'bot-spam':
                    await self.spam_logging_channel().send(embed=embed)
                else:
                    await self.event_logging_channel().send(embed=embed)
                    if before.channel.name == 'bot-spam':
                        await self.spam_logging_channel().send(embed=embed)
                    else:
                        await self.event_logging_channel().send(embed=embed)


def setup(bot):
    bot.add_cog(EventLoggerPlugin(bot))
