from discord.ext import commands

"""
BasePlugin

Used as a plugin base for all VEGAS Plugins
"""

class BasePlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def _check_user_exists(self, user_id, guild_id):
        """Private method to ensure a users discord_user_discorduser record exists.

        This method accepts a users id and a guild id. The SQL that gets executed
        will first check to see if a record for this user/guild exists. If the
        record exsists, that id is returned. If no record exists, a new record is
        created with the user/guild id combo and that new id is returned.

        Parameters
        ----------
        user_id: int
            The discord.Member.id of a guild member.
        guild_id: int
            A discord.Guild.id.

        Returns
        -------
        link_id: int
            The id of the users discord_user_discorduser record
        """
        sql_insert = """with s as (
                            select id
                            from discord_user_discorduser
                            where discord_guild_id = '%s' and discord_user_id = '%s'
                        ), i as (
                            insert into discord_user_discorduser ("discord_guild_id", "discord_user_id")
                            select '%s', '%s'
                            where not exists (select 1 from s)
                            returning id
                        )
                        select id
                            from i
                        union all
                        select id
                            from s"""
        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(sql_insert, (guild_id, user_id, guild_id, user_id))
                link_id = await cursor.fetchone()
                return link_id[0]
