"""This file provides multiple helper Python functions"""
import asyncio
import typing
from operator import attrgetter

from discord.utils import get


def asnyc_to_sync(coro: typing.Coroutine) -> typing.Any:
    """Allows async functions to run in non-async functions"""
    task = asyncio.create_task(coro)
    asyncio.get_running_loop().run_until_complete(task)
    return task.result()
