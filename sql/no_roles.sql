--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Ubuntu 12.4-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.4 (Ubuntu 12.4-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: discord_no_role_tempnorole; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discord_no_role_tempnorole (
    id integer NOT NULL,
    discord_user_id bigint NOT NULL,
    no_roles character varying(255) NOT NULL,
    remove_at timestamp with time zone NOT NULL,
    reason text NOT NULL
);


ALTER TABLE public.discord_no_role_tempnorole OWNER TO postgres;

--
-- Name: discord_no_role_tempnorole_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discord_no_role_tempnorole_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discord_no_role_tempnorole_id_seq OWNER TO postgres;

--
-- Name: discord_no_role_tempnorole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discord_no_role_tempnorole_id_seq OWNED BY public.discord_no_role_tempnorole.id;


--
-- Name: discord_no_role_tempnorole id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_no_role_tempnorole ALTER COLUMN id SET DEFAULT nextval('public.discord_no_role_tempnorole_id_seq'::regclass);


--
-- Name: discord_no_role_tempnorole discord_no_role_tempnorole_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discord_no_role_tempnorole
    ADD CONSTRAINT discord_no_role_tempnorole_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

